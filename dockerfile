FROM node:14.5.0
LABEL name="back-end-demo"
# 当前目录下所有文件 都拷贝到/app文件下
COPY . /app
# 进入到工作目录中
WORKDIR /app
# 安装依赖
RUN npm install
# 暴露端口
EXPOSE 3000
# 启动项目
CMD npm start
