/*
 * @Author: 望海潮
 * @Date: 2021-10-21 10:31:39
 * @LastEditTime: 2021-10-23 22:50:59
 * @Description: 
 */
const http = require('http')

const users = [
  { id: 1, name: '张三' },
  { id: 2, name: '李四' },
  { id: 3, name: '王二' }
]

const server = http.createServer(function(req, res) {
  res.setHeader('Access-Control-Allow-Origin','*')
  if(req.url === "/api/users") {
    res.end(JSON.stringify(users))
  } else {
    res.end('not found')
  }
})

server.listen(3000, () => {
  console.log('3000 port is running');
})
