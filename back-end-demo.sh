###
 # @Author: 望海潮
 # @Date: 2021-10-21 10:57:41
 # @LastEditTime: 2021-10-21 19:33:35
 # @Description: 
### 

#!/bin/bash
WORK_PATH="/var/jenkins_home/workspace/deploy-demo"
cd $WORK_PATH
echo "-----------clean old code"
# git reset --hard origin/master
# git clean -f
echo "-----------pull new code"
# git pull origin master
echo "-----------start build"
# docker build -t [要创建的镜像名称] [Dockerfile所在目录]
docker build -t back-end-demo-image .
echo "-----------stop/remove container"
# 停止容器、删除容器
docker stop back-end-demo-container
docker rm back-end-demo-container
echo "-----------run new container"
# 启动新容器
docker container run -p 3000:3000 --name back-end-demo-container -d back-end-demo-image
